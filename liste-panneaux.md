Bienvenue dans le monde de Minetest ! Petit points sur les controles:
Z pour avancer
S pour reculer
Q pour aller à gauche
D pour aller à droite
La souris pour orienter la vue
Click droit pour ouvrir/fermer les portes
Monte à l'étage pour découvrir la suite !

Niveau 1: Entrainement aux déplacements, suis le chemin et ne tombes pas !

Niveau 2: Entrainement aux sauts, grimpe tout en haut en évitant la lave !
Touche ESPACE pour sauter

Continue de courir ! Tu ne peux pas tomber dans les petits trous sauf si tu t'arrête

Niveau 3: Entrainement aux échelles, les échelles servent à grimper en hauteur et se rattraper lors des sauts
Maintient ESPACE pour grimper aux echelles
Maintient ESPACE lors d'un saut pour se rattraper sur une echelle
